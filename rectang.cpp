/* GCC */

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <vector>
#include <utility>
#include <list>
#include <iostream>
using namespace std;

#define MP(x, y) make_pair((x), (y))
#define X first
#define Y second
#define FOR(i, n) for (int i = 0; i < (n); ++i)

typedef pair<int, int> pii;
int n;
pii input[20];
bool used[20] = {};

int height, width;

list<pii> line;
typedef list<pii>::iterator iter;

struct pos
{
	pii cord;
	int id;
	bool turned;
};

pos sol[20];

iter find_cord()
{
	iter mn = line.begin();
	iter i = line.begin();
	for (; i != line.end(); ++i) {
		if (i->Y == height)
			continue;
		if (mn->Y == height)
			mn = i;
		if (mn->X > i->X || mn->X == i->X && mn->Y > i->Y)
			mn = i;
	}
	return mn;
}

void show()
{/*
	for (iter i = line.begin(); i != line.end(); ++i)
		cerr << i->X << " " << i->Y << "\n";
	cerr << "\n\n" << flush;*/
}

bool insert(int id, const iter& pos, bool turned)
{

	int w = input[id].X;
	int h = input[id].Y;
	if (turned)
		swap(w, h);

	iter prev(pos);
	iter next(pos);
	++next;
	--prev;

	if (pos->Y + h > height)
		return false;
	if (pos->X + w > width)
		return false;
	if (next->X - pos->X < w)
		return false;
	
	iter p1 = line.insert(pos, MP(pos->X, pos->Y + h));
	iter p2 = line.insert(pos, MP(pos->X + w, pos->Y + h));
	iter p3 = line.insert(next, MP(pos->X + w, pos->Y));

	line.erase(pos);
	if (*prev == *p1)
		line.erase(p1);
	if (*next == *p3)
		line.erase(p3);
	return true;
}

void remove(int id, pii pos, bool turned)
{
	int w = input[id].X;
	int h = input[id].Y;
	if (turned)
		swap(w, h);

	pii top = MP(pos.X + w, pos.Y + h);
	iter itop = find(line.begin(), line.end(), top);
	iter ileft = find(line.begin(), line.end(), MP(pos.X, pos.Y + h));
	iter iright = find(line.begin(), line.end(), MP(pos.X + w, pos.Y));
	line.erase(itop);
	line.insert(iright, MP(pos.X, pos.Y));

	if (ileft != line.begin()) {
		iter t = ileft;
		--t;
		if (t->X == ileft->X)
			line.erase(ileft);
	}
	if (iright != (--line.end())) {
		iter t = iright;
		++t;
		if (t->Y == iright->Y)
			line.erase(iright);
	}
}
int COUNT = 0;

void backtrack(int i)
{
	if (i == n) {
		FOR(j, n) {
			printf("%d %d %d %d%s", sol[j].cord.X, sol[j].cord.Y, sol[j].id + 1, sol[j].turned, ((j != n - 1) ? " " : "\n"));
			++COUNT;
		}
		return;
	}
	FOR(j, n) {
		if (!used[j]) {
			iter cord = find_cord();
			int cordx = cord->X;
			int cordy = cord->Y;
			//show();
			if (insert(j, cord, false)) {
				used[j] = true;
				sol[i].cord = MP(cordx, cordy);
				sol[i].id = j;
				sol[i].turned = 0;
				show();
				backtrack(i + 1);
				used[j] = false;
				remove(j, MP(cordx, cordy), false);
				show();
			}
			cord = find_cord();
			cordx = cord->X;
			cordy = cord->Y;
			if (insert(j, cord, true)) {
				used[j] = true;
				sol[i].cord = MP(cordx, cordy);
				sol[i].id = j;
				sol[i].turned = 1;
				show();
				backtrack(i + 1);
				used[j] = false;
				remove(j, MP(cordx, cordy), true);
				show();
			}
		}
	}

}

int main()
{
	freopen("rectang.in", "r", stdin);
	freopen("rectang.out", "w", stdout);

	scanf("%d", &n);
	FOR(i, n) {
		int a, b;
		scanf("%d%d", &a, &b);
		input[i] = MP(a, b);
	}
	scanf("%d%d", &width, &height);
	int S = 0;

	FOR(i, n) {
		S += input[i].X*input[i].Y;
		if (min(input[i].X, input[i].Y) > min(width, height) || max(input[i].X, input[i].Y) > max(width, height)) {
			printf("No solution\n");
			return 0;
		}
	}
	if (S != width*height) {
		printf("No solution\n");
		return 0;
	}
	line.push_back(MP(0, height));
	line.push_back(MP(0, 0));
	line.push_back(MP(width, 0));
	backtrack(0);
	if (COUNT == 0) {
		printf("No solution\n");
		return 0;
	}
	return 0;
}