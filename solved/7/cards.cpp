/* GCC */

#include <cstdio>
using namespace std;

int a, b;

int prev(int p, int n)
{
    int L = a*n/b;

    if (p < 1+L)
        return 2*p-1;
    if (p < n)
        return 2*p+1;
    if (p < n+L)
        return 2*(p-(n-1));
    return 2*(p-(n-1))+2;
}

int main()
{
    freopen("cards.in", "r", stdin);
    freopen("cards.out", "w", stdout);

    int n;
    scanf("%d%d%d", &n, &a, &b);

    int x = 1, y = 2;

    for (int i =2; i<=n; ++i) {
        x = prev(x, i);
        y = prev(y, i);
    }

    printf("%d %d\n", x, y);

    return 0;
}
