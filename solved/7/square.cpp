/* GCC */

#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <cstring>

using namespace std;

int a[210][210];
int state[110][110];
int sym[4];
bool numsdifferent;
vector<int> moves;
int invSym;
int n;

void swap3(int&a, int&b, int&c)
{
    int t = a;
    a = b;
    b = c;
    c = t;
}

int invSort(int s[])
{
    int inv = 0;
    for (int i = 0; i < 4; ++i)
    {
        for (int j = i+1; j < 4; ++j)
        {
            if (s[i] > s[j])
            {
                inv++;
                swap(s[i], s[j]);
            }
        }
    }

    return inv;
}

void fill(int t[], int y, int x)
{
    t[0] = a[y][x];
    t[1] = a[y][2*n-1-x];
    t[2] = a[2*n-1-y][x];
    t[3] = a[2*n-1-y][2*n-1-x];
}

int getState(int y, int x)
{
    int t[4];
    fill(t, y, x);
    int in = invSort(t);
    if (t[0] != sym[0] || t[1] != sym[1] || t[2] != sym[2] || t[3] != sym[3])
        return 2;
    if (numsdifferent)
        return in % 2;
    else
        return 0;
}

void pushUnique()
{
    int t[4];
    fill(t, 0, 0);
    int freq[4] = {};

    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            if (t[i] == t[j])
                ++freq[i];
        }
    }
    int ind = -1;
    for (int i=0; i<4; ++i)
        if (freq[i] == 1)
        {
            ind = i;
            break;
        }
    if (ind == -1 || ind == 0)
        return;
    if (ind == 1)
    {
        moves.push_back(1);
        reverse(a[0], a[0]+2*n);
    }
    else if (ind == 2)
    {
        moves.push_back(-1);
        for (int k = 0, j = 2*n-1; k < j; ++k, --j)
        {
            swap(a[k][0], a[j][0]);
        }
    }
    else if (ind == 3)
    {
        moves.push_back(2*n);
        moves.push_back(-1);
        reverse(a[2*n-1], a[2*n-1]+2*n);
        for (int k = 0, j = 2*n-1; k < j; ++k, --j)
        {
            swap(a[k][0], a[j][0]);
        }
    }
}

void solve()
{
    fill(sym, 0, 0);

    invSym = invSort(sym)%2;

    numsdifferent = true;
    if (sym[0] == sym[1] || sym[1] == sym[2] || sym[2] == sym[3])
        numsdifferent = false;

    if (!numsdifferent)
    {
        pushUnique();
        fill(sym, 0, 0);
        invSym = invSort(sym)%2;
    }

    for (int i =0; i < n; ++i)
    {
        for (int j=0; j < n; ++j)
        {
            state[i][j] = getState(i, j);
            if (state[i][j] == 2)
            {
                printf("-1\n");
                return;
            }
        }
    }
    if (numsdifferent)
    {
        for (int i = 0; i < n; ++i)
        {
            if (state[i][0] != invSym)
            {
                moves.push_back(i+1);
                //reverse(state[0], state[0]+n);
                reverse(a[i], a[i]+2*n);
                for (int j = 0; j < n; ++j)
                {
                    state[i][j] ^= 1;
                }
            }
        }

        for (int i=0; i<n; ++i)
        {
            if (state[0][i] != invSym)
            {
                moves.push_back(-(i+1));
                for (int k = 0; k < n; ++k)
                {
                    state[k][i] ^= 1;
                }
                for (int k = 0, j = 2*n-1; k < j; ++k, --j)
                {
                    swap(a[k][i], a[j][i]);
                }
            }
        }

        for (int i=0; i<n; ++i)
        {
            for (int j=0; j<n; ++j)
            {
                if (state[i][j] != invSym)
                {
                    printf("-1\n");
                    return;
                }
            }
        }
    }

    fill(sym, 0, 0);

    for (int i = 0; i<n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            int t[4];
            fill(t, i, j);
            if (t[0] != sym[0])
            {
                if (t[1] == sym[0])
                {
                    swap3(a[i][j], a[2*n-i-1][2*n-j-1], a[i][2*n-j-1]);
                    swap3(a[i][j], a[2*n-i-1][2*n-j-1], a[i][2*n-j-1]);
                    moves.push_back(-(2*n-j));
                    moves.push_back(i+1);
                    moves.push_back(-(2*n-j));
                    moves.push_back(i+1);

                }
                else if (t[2] == sym[0])
                {
                    swap3(a[i][j],  a[2*n-i-1][j],  a[2*n-i-1][2*n-j-1]);
                    // swap3(a[i][j], a[2*n-i-1][j],  a[2*n-i-1][2*n-j-1]);
                    moves.push_back((2*n-i));
                    moves.push_back(-(j+1));
                    moves.push_back((2*n-i));
                    moves.push_back(-(j+1));

                }
                else if (t[3] == sym[0])     // debugged
                {
                    swap3(a[i][j], a[2*n-i-1][j],  a[2*n-i-1][2*n-j-1]);
                    swap3(a[i][j], a[2*n-i-1][j],  a[2*n-i-1][2*n-j-1]);
                    moves.push_back(-(j+1));
                    moves.push_back((2*n-i));
                    moves.push_back(-(j+1));
                    moves.push_back((2*n-i));
                }
            }

            while (memcmp(t, sym, 4*4) != 0)
            {
                swap3(a[2*n-i-1][2*n-j-1], a[i][2*n-1-j], a[2*n-i-1][j]);
                moves.push_back(-(2*n-j));
                moves.push_back((2*n-i));
                moves.push_back(-(2*n-j));
                moves.push_back((2*n-i));
                fill(t, i, j);
            }
        }
    }
    printf("%d\n", (int)moves.size());
    for (int i =0; i<(int)moves.size(); ++i)
    {
        if (moves[i] > 0)
            printf("+");
        printf("%d\n", moves[i]);
    }
}

int main()
{
    freopen("square.in", "r", stdin);
    freopen("square.out", "w", stdout);

    int t;
    scanf("%d", &t);

    for (int tt=0; tt<t; ++tt)
    {
        moves.clear();
        scanf("%d", &n);
        for (int i =0; i<2*n; ++i)
        {
            for (int j=0; j<2*n; ++j)
            {
                scanf("%d", &a[i][j]);
            }
        }
        solve();
    }

    return 0;
}
