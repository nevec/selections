/* GCC */

#include <vector>
#include <stdio.h>
#include <stdint.h>
#include <cmath>
#include <fstream>

typedef int64_t i64;

const int Nmax = 260000;
const int Inf = 2e9;

int N;
int x[Nmax], y[Nmax], left[Nmax], right[Nmax];

std::vector<int> s;

inline i64 dist(int v, int u)
{
    if (v == -1 || u == -1)
        return 0;
    else
        return std::abs(y[v]-y[u]) + std::abs(x[v]-x[u]);
}

int main()
{
    freopen("nests.in", "r", stdin);

    scanf("%d", &N);
    for (int i=1;i<=N;++i) {
        scanf("%d%d", &x[i], &y[i]);
        left[i] = right[i] = -1;
    }

    s.push_back(0);
    y[0] = Inf;

    for (int i=1;i<=N;++i){
        while(y[s.back()] < y[i]) {
            left[i] = s.back();
            s.pop_back();
        }
        right[s.back()] = i;
        s.push_back(i);
    }

    i64 ans = 0;

    for (int i=1;i<=N;++i)
        ans += dist(i, left[i]) + dist(i, right[i]);

    std::ofstream fout("nests.out");
    fout << ans << "\n";

    return 0;
}
