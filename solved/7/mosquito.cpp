/* GCC */

#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

struct Point
{
    int x, y;
    Point() : x(0), y(0) {}
    Point(int x, int y): x(x), y(y) {}
};

bool operator<(Point l, Point r) {return l.x < r.x || (l.x == r.x && l.y < r.y);}

const int Nmax = 100100;

int n;
vector<Point> points;
vector<int> u;

int C[4*Nmax],
    M[4*Nmax];

void update(int v, int l, int r, int L, int R, int x)
{
    if (l == L && r == R) {
        M[v] += x;
        C[v] += x;
        return;
    }
    int m = (L+R)>>1;
    if (l <= m && r <= m) {
        update(v*2, l, r, L, m, x);
    } else if (l > m && r > m) {
        update(v*2+1, l, r, m+1, R, x);
    } else {
        update(v*2, l, m, L, m, x);
        update(v*2+1, m+1, r, m+1, R, x);
    }
    M[v] = max(M[2*v],M[2*v+1])+C[v];
}

int MAX() {return M[1];}

void add_range(int l, int r) { update(1, l, r, 0, n+5, 1);}
void sub_range(int l, int r) { update(1, l, r, 0, n+5, -1);}

int upper(int y) {return distance(u.begin(), upper_bound(u.begin(), u.end(), y)); }

int f(int w, int h)
{
    memset(C, 0, Nmax*4*4);
    memset(M, 0, Nmax*4*4);

    int xleft = points[0].x;
    int left = 0, right = 0;

    while (right < n && points[right].x - xleft <= w) {
        add_range(upper(points[right].y), upper(points[right].y + h));
        ++right;
    }

    int opt = MAX();

    while (right < n) {
        while (points[left].x == xleft) {
            sub_range(upper(points[left].y), upper(points[left].y+h));
            ++left;
        }
        xleft = points[left].x;

        while (right < n && points[right].x - xleft <= w) {
            add_range(upper(points[right].y), upper(points[right].y + h));
            ++right;
        }
        opt = max(opt, MAX());
        if (opt == n || opt == (w+1)*(h+1))
            break;
    }
    return opt;
}

int main()
{
    freopen("mosquito.in", "r", stdin);
    freopen("mosquito.out", "w", stdout);

    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        int x, y;
        scanf("%d%d", &x, &y);
        points.push_back(Point(x, y));
        u.push_back(y);
    }
    int w, h;
    scanf("%d%d", &w, &h);

    sort(points.begin(), points.end());
    sort(u.begin(), u.end());

    u.resize(distance(u.begin(), unique(u.begin(), u.end())));

    int ans = f(w, h);
    ans = max(ans, f(h, w));

    printf("%d\n", ans);

    return 0;
}
