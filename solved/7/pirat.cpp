/* GCC */

#include <cstdio>

using namespace std;

int a[10010] = {};

void get_r(int n, int m, int k)
{
    for (int i=k;i<n&&m>0;i += 2) {
        a[i] = 1;
        --m;
    }
    a[n-1] += m;
}

void get_R(int n, int m)
{
    if (m >= (n-1)/2) {
        get_r(n, m, 1-n%2);
        return;
    }
    int k = (m+1)*2;
    int c = 0;
    int v = 1-k%2;

    for (int i=k+1;i<=n;++i){
        if (m+c >= (i-1)/2){
            c = 0;
            v ^= 1;
        } else
            ++c;
    }

    get_r(n, m, v);
    for (int i = n-1, j = c; j > 0; --j, --i)
        a[i] = -1;
}

int main()
{
    freopen("pirat.in", "r",stdin);
    freopen("pirat.out", "w", stdout);

    int n, m;
    scanf("%d%d", &n, &m);
    get_R(n, m);

    for (int i=0;i<n;++i)
        printf("%d\n", a[i]);

    return 0;
}
