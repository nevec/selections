/* GCC */

#include <cstdio>
#include <algorithm>
#include <vector>
#include <stdint.h>
#include <iostream>
#include <cmath>

using namespace std;

typedef vector<int> bigint;
typedef int64_t i64;

void show(const bigint&);

void normalize(bigint& b)
{
    int shift = 0;
    for (int i = 0; i < b.size(); ++i) {
        b[i] += shift;
        shift = b[i] / 10;
        b[i] %= 10;
    }
    while (shift != 0) {
        b.push_back(shift % 10);
        shift /= 10;
    }
    while (b.size() > 1 && b.back() == 0)
        b.pop_back();
}

bigint operator + (const bigint& a, const bigint& b)
{
    bigint c(max(a.size(), b.size()), 0);

    for (int i = 0; i < c.size(); ++i) {
        if (i < a.size())
            c[i] += a[i];
        if (i < b.size())
            c[i] += b[i];
    }
    normalize(c);
    return c;
}

bigint operator * (const bigint& a, const bigint& b)
{
    bigint c(a.size()+b.size(), 0);

    for (int i =0; i<a.size();++i) {
        for (int j=0; j<b.size(); ++j)
            c[i+j] += a[i]*b[j];
    }
    normalize(c);
    return c;
}

bigint operator * (const bigint& a, int s)
{
    bigint c = a;
    for (int i = 0; i < a.size(); ++i)
        c[i] *= s;
    normalize(c);
    return c;
}

const bigint ONE = bigint(1, 1);

bigint Cnk[101][101];

bigint C(int n, int k)
{
    if (!Cnk[n][k].empty())
        return Cnk[n][k];
    if (n == k || k == 0) {
        return Cnk[n][k] = ONE;
    }

    return Cnk[n][k] = C(n-1, k-1) + C(n-1, k);
}

vector<bigint> F;

bigint ff(int n)
{
    if (F.size()> n)
        return F[n];
    if (F.empty()) {
        F.push_back(ONE);
        F.push_back(ONE);
        F.push_back(ONE);
    }

    for (int i = F.size(); i <= n; i += 2) {
        F.push_back(F[i-2]*i);
        F.push_back(bigint());
    }
    return F[n];
}

bigint Pow(int x, int p)
{
    bigint ans = ONE;
    for (int i = 0; i < p; ++i)
        ans = ans * x;
    return ans;
}

void print(const bigint& c)
{
    for (int i = c.size()-1; i >= 0; --i)
        printf("%d", c[i]);
    printf("\n");
}

int main()
{
    freopen("sleep2.in", "r", stdin);
    freopen("sleep2.out", "w", stdout);

    int n;
    scanf("%d", &n);

    vector<int> perm;
    for (int i =0;i<n;++i) {
        int t;
        scanf("%d", &t);
        perm.push_back(t-1);
    }

    int cycles[101] = {};
    bool used[101] = {};

    for (int i = 0; i <n;++i) {
        if (!used[i]) {
            int v = i;
            int k = 0;
            while (!used[v]) {
                used[v] = true;
                ++k;
                v = perm[v];
            }
            cycles[k]++;
        }
    }

    bigint ans = ONE;

    for (int i = 1; i <= n; ++i) {
        if (cycles[i] > 0) {
            if (i%2 == 0 && cycles[i]%2 == 1) {
                printf("0\n");
                return 0;
            }
            if (i%2 == 0) {
                ans = ans*ff(cycles[i]-1)*Pow(i, cycles[i]/2);
            } else {
                bigint t = ONE;
                for (int j=1; j <= cycles[i]/2; ++j) {
                   t = t + C(cycles[i], 2*j)*Pow(i, j)*ff(2*j-1);
                }
                ans = ans*t;
            }
        }
    }

    print(ans);
    return 0;
}
