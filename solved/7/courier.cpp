/* GCC */

#include <cstdio>
#include <algorithm>
#include <stdint.h>
#include <vector>
using namespace std;

const int Nmax = 100100;

int main()
{
    freopen("courier.in", "r", stdin);
    freopen("courier.out", "w", stdout);

    int n, m;
    scanf("%d%d", &n, &m);
    vector<int> x, y;

    for (int i=0; i<n; ++i) {
        int t;
        scanf("%d", &t);
        x.push_back(t);
    }

    for (int i=0; i<m; ++i) {
        int t;
        scanf("%d", &t);
        y.push_back(t);
    }

    int64_t sum = 0;
    sort(x.begin(), x.begin()+n);

    for (int i=0; i<m; ++i)
    {
        vector<int>::iterator it1 = lower_bound(x.begin(), x.begin()+n, y[i]);
        if (it1 == x.begin())
        {
            sum += abs(*it1-y[i]);
            *it1 = y[i];
        }
        else if (it1 == x.begin()+n)
        {
            sum += abs(x[n-1]-y[i]);
            x[n-1] = y[i];
        }
        else
        {
            vector<int>::iterator it2 = it1-1;
            if (abs(*it1-y[i]) < abs(*it2-y[i])) {
                sum += abs(*it1-y[i]);
                *it1 = y[i];
            } else {
                sum += abs(*it2-y[i]);
                *it2 = y[i];
            }
        }
    }

    printf("%I64d\n", sum);

    return 0;
}
