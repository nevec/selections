/* GCC */

#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

#define X first
#define Y second
#define MP(x, y) make_pair((x), (y))

const int Nmax = 100100;

int n, m;
vector<pair<int, int> > graph[Nmax];

int cost[Nmax];
int profitUsed[Nmax],
    profitNotUsed[Nmax],
    used[Nmax],
    id[Nmax];

bool visited[Nmax] = {};
int components[Nmax] = {};

void dfs(int v, int k, int comp)
{
    id[v] = k;
    components[v] = comp;

    visited[v] = true;

    for (int i=0;i<graph[v].size(); ++i) {
        int to = graph[v][i].first;
        if (!visited[to]) {
            dfs(to, k+1, comp);
        }
    }
}

int dfsNotUsed(int v, bool mark);

int dfsUsed(int v, bool mark)
{
    if (profitUsed[v] != -1 && !mark)
        return profitUsed[v];

    int sum = cost[v];

    if (mark)
        used[v] = true;

    for  (int i=0;i<graph[v].size(); ++i) {
        int to = graph[v][i].first;
        int edgeCost = graph[v][i].second;

        if (id[v] < id[to]) {
            if (mark) {
                if (dfsNotUsed(to, false) > dfsUsed(to, false) - edgeCost) {
                    dfsNotUsed(to, true);
                } else {
                   // used[to] = true;
                    dfsUsed(to, true);
                }
            }
            else
                sum += max(dfsNotUsed(to, false), dfsUsed(to, false) - edgeCost);

        }
    }
    if (!mark) {
        profitUsed[v] = sum;
        return sum;
    }else
        return profitUsed[v];
}

int dfsNotUsed(int v, bool mark)
{
    if (profitNotUsed[v] != -1 && !mark)
        return profitNotUsed[v];

    int sum = 0;

    for  (int i=0;i<graph[v].size() ; ++i) {
        int to = graph[v][i].first;

        if (id[v] < id[to]) {
            if (mark) {
                if (dfsNotUsed(to, false) > dfsUsed(to, false)) {
                    dfsNotUsed(to, true);
                } else {
                    //used[to] = true;
                    dfsUsed(to, true);
                }
            } else
                sum += max(dfsNotUsed(to, false), dfsUsed(to, false));
        }
    }

    if (!mark) {
        profitNotUsed[v] = sum;
        return sum;
    } else
        return profitNotUsed[v];
}

int main()
{
    freopen("islands.in", "r", stdin);
    freopen("islands.out", "w", stdout);

    scanf("%d%d", &n, &m);
    for (int i=0;i<n; ++i) {
        scanf("%d", &cost[i]);
    }
    for (int i=0;i<m;++i){
        int a, b, c;
        scanf("%d%d%d", &a, &b, &c);
        --a;
        --b;
        graph[a].push_back(MP(b, c));
        graph[b].push_back(MP(a, c));
    }

    fill(used, used+n, false);
    fill(profitUsed, profitUsed+n, -1);
    fill(profitNotUsed, profitNotUsed+n, -1);

    int ans = 0;
    int component = 1;

    for (int i=0;i<n; ++i) {
        if (!visited[i]) {
            dfs(i, 0, component);
            ++component;
        }
    }

    int c = 1;
    for (int i=0;i<n; ++i) {
        if (components[i] == c) {
            ans += max(dfsNotUsed(i, false), dfsUsed(i, false));
            ++c;
            if (dfsNotUsed(i, false) > dfsUsed(i, false))
                dfsNotUsed(i, true);
            else
                dfsUsed(i, true);
        }
    }


    printf("%d\n", ans);

    printf("%d", (int)count(used, used+n, true));
    for (int i=0;i<n;++i)
        if (used[i])
            printf(" %d", i+1);
    printf("\n");
    return 0;
}
