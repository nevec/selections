/* GCC */

#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> bigint;

void normalize(bigint& c)
{
    int shift = 0;

    for (int i = 0;i <c.size();++i) {
        c[i] += shift;
        shift = c[i]/10;
        c[i] %= 10;
    }

    while (shift != 0) {
        c.push_back(shift % 10);
        shift /= 10;
    }

    while (c.size() > 1 && c.back() == 0)
        c.pop_back();
}

bigint operator *=(bigint& a, int x)
{
    for (int i =0;i<a.size();++i)
        a[i] *= x;
    normalize(a);
    return a;
}

const bigint ONE = bigint(1, 1);

bool isPrime(int x)
{
    for (int i = 2; i*i <= x; ++i)
        if (x % i == 0)
            return false;
    return true;
}

int F(int p, int k)
{
    int ans = 0;
    for (int i = p; i <= k; i *= p)
        ans += k/i;
    return ans;
}

void show(const bigint& c)
{
    for (int i = c.size()-1;i>=0;--i)
        printf("%d", c[i]);
    printf("\n");
}

int main()
{
    freopen("convol.in","r", stdin);
    freopen("convol.out", "w",stdout);

    int n;
    scanf("%d", &n);
    --n;

    bigint ans = ONE;
    for (int i=2;i<=2*n;++i) {
        if (isPrime(i)) {
            int k = F(i, 2*n) - 2*F(i, n);
            for (int j=0;j<k; ++j)
                ans *= i;
        }
    }

    show(ans);

    return 0;
}
