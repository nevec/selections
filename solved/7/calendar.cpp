/* GCC */

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <climits>
using namespace std;

const int Nmax = 100, Mmax = 12;

int n, m;
int c[Nmax+1][Mmax];
int a[2][(1 << Mmax)] = {};

void f(int i, int current_state, int bit, int previous_state, int sum)
{
    if (bit == m || (bit == m-1 && !(previous_state&(1<<bit))) )
    {
        if (a[i%2][current_state] < a[(i+1)%2][previous_state] + sum)
            a[i%2][current_state] = a[(i+1)%2][previous_state] + sum;
    }
    else
    {
        if (bit == 0 && (current_state&1) && (c[i][0] + c[i-1][m-1] > 0))
            f(i, current_state, bit+1, previous_state^(1 << (m-1)), sum + c[i][0] + c[i-1][m-1]);
        if ((current_state&(1<<bit))) {
            if (bit < m-1 && (current_state&(1<<(bit+1)) && (c[i][bit] + c[i][bit+1]) > 0))
                f(i, current_state, bit+2, previous_state, sum + (c[i][bit] + c[i][bit+1]));
            if ((c[i][bit] + c[i-1][bit] > 0))
                f(i, current_state, bit+1, previous_state^(1<<bit), sum+c[i][bit] + c[i-1][bit]);
        }
        f(i, current_state, bit+1, previous_state, sum);
    }
}

int main()
{
    freopen("calendar.in", "r", stdin);
    freopen("calendar.out", "w", stdout);

    scanf("%d%d", &n, &m);

    for (int i=1; i<=n; ++i)
    {
        for (int j=0; j<m; ++j)
            scanf("%d", &c[i][j]);
    }

    fill(a[0], a[0]+(1<<m), INT_MIN);
    a[0][(1<<m)-1] = 0;

    for (int j = 1; j <= n; ++j) {
        for (int i=0;i<(1<<m); ++i) {
            a[j%2][i] = 0;
            f(j, i, 0, (1<<m)-1, 0);
        }
    }

    printf("%d\n", *max_element(a[n%2], a[n%2]+(1<<m)));

    return 0;
}
