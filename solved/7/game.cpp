/* GCC */

#include <vector>
#include <algorithm>
#include <cstring>
#include <set>
#include <cstdio>
using namespace std;

int n, m;
const int Max = 22;

bool I[Max] = {}, J[Max] = {};

int g[Max][Max][Max][Max] = {};

int mex(set<int>& s)
{
    for (int i = 0; ; ++i)
    {
        if (s.find(i) == s.end())
            return i;
    }
}

int fn(int x0, int x1, int y0, int y1)
{
    if (x0 >= x1 || y0 >= y1)
        return 0;
    if (g[x0][x1][y0][y1] != -1)
        return g[x0][x1][y0][y1];

    set<int> pos;
    for (int i = x0; i < x1; ++i)
    {
        for (int j= y0; j < y1; ++j)
        {
            if ((I[i] ^ J[j]) == 0)
            {
                pos.insert((fn(x0, i, y0, j) ^ fn(i+1, x1, j+1, y1)));
            }
        }
    }

    g[x0][x1][y0][y1] = mex(pos);
    return g[x0][x1][y0][y1];
}

int main()
{
    freopen("game.in", "r", stdin);
    freopen("game.out", "w", stdout);
    //ios_base::sync_with_stdio(false);


    scanf("%d%d", &m, &n);
    for (int i=0; i<m; ++i)
    {
        int t;
        scanf("%d", &t);
        I[i] = t;
    }
    for (int i=0; i<n; ++i)
    {
        int t;
        scanf("%d", &t);
        J[i] = t;
    }

    memset(g, -1, Max*Max*Max*Max*4);

    if (fn(0, m, 0, n))
        printf("WIN\n");
    else
    {
        printf("LOSE\n");
        return 0;
    }

    for (int i=0; i<m; ++i)
        for (int j=0; j<n; ++j)
            if ((I[i]^J[j]) == 0 && (fn(0,i,0,j)^fn(i+1, m, j+1, n)) == 0)
                printf("%d %d\n", i, j);



    return 0;
}
