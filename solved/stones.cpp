/* GCC */

#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

int pos[1000][5];
int val[1000];

int l[5];

int n, m, n_pos;

int find(const int state[])
{
    for (int i = 0; i < n_pos; ++i) {
        if (memcmp(state, pos[i], 5*4) == 0) {
            return i;
        }
    }
    memcpy(pos[n_pos], state, 5*4);
    return n_pos++;
}

void solve(int u, bool show_info)
{
    if (val[u] != -1 && !show_info)
        return;
    int state[5];
    memcpy(state, pos[u], 5*4);
    int looses = 0;

    for (int i = 0; i < n-1; ++i) {
        for (int j = 0; j < m; ++j) {
            if (state[i] >= l[j]) {
                state[i] -= l[j];
                state[i+1] += l[j];
                int to = find(state);
                solve(to, false);
                if (!val[to]) {
                    looses++;
                    if (show_info) {
                        printf("%d", state[0]);
                        for (int ii = 1; ii < n; ++ii) {
                            printf(" %d", state[ii]);
                        }
                        printf("\n");
                    }
                }
                state[i] += l[j];
                state[i+1] -= l[j];
            }
        }
    }

    val[u] = (looses != 0);
}

int main()
{
    freopen("stones.in", "r", stdin);
    freopen("stones.out", "w", stdout);

    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; ++i) {
        scanf("%d", &pos[0][i]);
    }
    for (int i = 0; i < m; ++i) {
        scanf("%d", &l[i]);
    }
    memset(val, -1, 1000*4);
    n_pos = 1;

    solve(0, false);

    if (val[0]) {
        puts("1");
        solve(0, true);
    } else {
        puts("2");
    }


	return 0;
}
