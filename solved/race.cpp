/* GCC */

#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <queue>
using namespace std;

vector<vector<int> > g;

vector<bool> mark;

int start, finish;
int n;

bool bfs_find(int v)
{
	queue<int> q;
	q.push(start);
	vector<char> used(n, false);
	used[start] = true;
	if (v == start)
		return true;
	while (!q.empty()) {
		int u = q.front();
		q.pop();
		if (u == v)
			continue;

		for (int i = 0; i < (int)g[u].size(); ++i) {
			int to = g[u][i];
			if (to != v && !used[to]) {
				used[to] = true;
				q.push(to);
			}
			if (to == finish)
				return true;
		}
	}

	return false;
}

void bfs_mark(int v)
{
	queue<int> q;
	q.push(v);

	//mark[v] = true;

	while (!q.empty()) {
		int u = q.front();
		q.pop();

		for (int i = 0; i < (int)g[u].size(); ++i) {
			int to = g[u][i];
			if (!mark[to]) {
				mark[to] = true;
				q.push(to);	
			}
		}
	}
}

bool bfs_check()
{
	queue<int> q;
	q.push(start);

	if (mark[start])
		return true;
	vector<bool> used(n, false);

	used[start] = true;

	while (!q.empty()) {
		int to = q.front();
		q.pop();

		for (int i = 0; i < (int)g[to].size(); ++i) {
			int u = g[to][i];

			if (mark[u])
				return true;
			if (!used[u]) {
				used[u] = true;
				q.push(u);
			}
		}
	}
	if (count(mark.begin(), mark.end(), 1) == 1 && mark[finish])
		return true;
	if (count(mark.begin(), mark.end(), 1) == n - 2 && !mark[start])
		return true;
	return false;
}

int main()
{
	int i = 0;

	FILE* fin = fopen("race.in", "r");
	char line[900];
	

	while (fgets(line, 900, fin) != NULL) {
		g.push_back(vector<int>());
		int t;
		char* num = strtok(line, " \n");
		
		while (num != NULL) {
			g[i].push_back(atoi(num)-1);
			num = strtok(NULL, " \n");
		}
		++i;
	}
	n = i;

	
	vector<signed char> used(n, 0);
	for (int i = 0; i < n; ++i) {
		if (g[i].empty())
			finish = i;
		for (int j = 0; j < (int)g[i].size(); ++j) {
			used[g[i][j]] = 1;
		}
	}
	start = find(used.begin(), used.end(), 0) - used.begin();

	vector<int> avoid;
	for (int i = 0; i < n; ++i) {
		if (!bfs_find(i))
			avoid.push_back(i);
	}

	mark.resize(n, false);

	vector<int> parts;
	for (int i = 0; i < (int)avoid.size(); ++i) {
		int v = avoid[i];
		fill(mark.begin(), mark.end(), false);

		vector<int> backup(g[v].begin(), g[v].end());
		bfs_mark(v);
		g[v].clear();
		if (!bfs_check())
			parts.push_back(v);

		g[v].assign(backup.begin(), backup.end());
	}
	fclose(fin);
	freopen("race.out", "w", stdout);
	printf("%d %d\n", start+1, finish+1);
	printf("%d", (int)avoid.size());
	for (int i = 0; i < (int)avoid.size(); ++i)
		printf(" %d", avoid[i] + 1);
	printf("\n");
	printf("%d", (int)parts.size());
	for (int i = 0; i < (int)parts.size(); ++i) {
		printf(" %d", parts[i] + 1);
	}
	printf("\n");


	return 0;
}