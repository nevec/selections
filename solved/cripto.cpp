/* GCC */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <utility>
#include <fstream>

using namespace std;

vector<string> words;
string res;
string line;
int n;
int mmap[256] = {};
vector<int> dict;
vector<vector<int> > nums;
vector<bool> used;
int mx;

inline int ord(char ch) { return (dict[mmap[ch]]-1) % n; }

void read()
{
	ifstream fin("cripto.in");
	//freopen("cripto.in", "r", stdin);
	//ios_base::sync_with_stdio(false);
	//cin.tie(0);
//#define fin cin

	fin >> n;
	char buff[100];
	fin >> buff;
	int b2[100];

	for (ptrdiff_t i = 0; i < 100; ++i) {
		b2[i] = buff[i];
	}

	for (int i = 0; b2[i] != 0U; ++i) {
		if (b2[i] < 0)
			b2[i] += 128;
	}
	char buff3[100];
	for (int i = 0; i < 100; ++i) {
		buff3[i] = (char)b2[i];
	}
	line = buff3;

	int offset = 0;
	while (1) {
		int i = line.find('+', offset);
		if (i == string::npos) {
			i = line.find('=', offset);
			words.push_back(line.substr(offset, i - offset));
			res = line.substr(i + 1);
			break;
		}
		words.push_back(line.substr(offset, i - offset));
		offset = i + 1;
	}
}

void parse()
{
	int code = 1;
	dict.push_back(0);

	for (int i = 0; i < (int)words.size(); ++i) {
		for (int j = 0; j < (int)words[i].size(); ++j)
			if (mmap[words[i][j]] == 0) {
				mmap[words[i][j]] = code;
				dict.push_back(code);
				++code;
			}
	}
	for (int j = 0; j < (int)res.size(); ++j)
		if (mmap[res[j]] == 0) {
			mmap[res[j]] = code;
			dict.push_back(code);
			++code;
		}
}

void prep()
{
	mx = -1;
	for (int i = 0; i < (int)words.size(); ++i) {
		if ((int)words[i].size() > mx)
			mx = words[i].size();
	}
	if ((int)res.size() > mx)
		mx = res.size();
	nums.resize(words.size() + 1, vector<int>(mx));
}

bool init()
{
	for (int i = 0; i < (int)words.size(); ++i) {
		int len = words[i].size();
		bool can_zero = false;
		for (int j = 0; j < len; ++j) {
			int digit = ord(words[i][j]);
			if (!can_zero && digit == 0)
				return false;
			if (digit != 0)
				can_zero = true;
			nums[i][mx - len + j] = digit;
		}
	}
	int len = res.size();
	bool can_zero = false;
	for (int j = 0; j < len; ++j) {
		int digit = ord(res[j]);
		if (!can_zero && digit == 0)
			return false;
		if (digit != 0)
			can_zero = true;
		nums[nums.size() - 1][mx - len + j] = ord(res[j]);
	}
	return true;
}

bool check()
{
	if (!init())
		return false;
	int shift = 0;

	for (int j = mx - 1; j >= 0; --j) {
		int sum = 0;
		for (int i = 0; i < (int)nums.size() - 1; ++i) {
			sum += nums[i][j];
		}
		sum += shift;
		if (sum % n != nums.back()[j])
			return false;
		shift = sum / n;
	}

	return shift == 0;
}

void backtrack(int i)
{
	if (i == (int)dict.size()) {
		if (check()) {
			for (int i = 0; i < (int)line.size(); ++i) {
				if (line[i] == '+' || line[i] == '=')
					cout << line[i];
				else
					cout << ord(line[i]);
			}
			cout << "\n";
		}
		return;
	}

	for (int j = n-1; j >= 0; --j) {
		if (!used[j]) {
			used[j] = true;
			dict[i] = j;
			backtrack(i + 1);
			used[j] = false;
		}
	}
}

void bydlokod()
{
	//int a[11] = {};
	bool used[11] = {};
	fill(dict.begin(), dict.end(), 0);
	int n = dict.size() - 1;
	int m = ::n;
	int i = 0;
	bool gone = false;
	int count = 0;

	//dict[0] = -1;
	//n = m = 3;

next:
	if (i == n) {
		--i;
		if (check()) {
			for (int i = 0; i < (int)line.size(); ++i) {
				if (line[i] == '+' || line[i] == '=')
					cout << line[i];
				else
					cout << ord(line[i]);
			}
			cout << "\n";
		}
	}


	for (int j = dict[i + 1] + 1; j <= m; ++j) {
		if (!used[j]) {
			used[dict[i + 1]] = false;
			dict[i + 1] = j;
			used[j] = true;
			++i;
			goto next;
		}
	}
	if (i != 0) {
		used[dict[i + 1]] = false;
		dict[i + 1] = 0;
		--i;
		goto next;
	}
}

int main()
{
	ios_base::sync_with_stdio(false);
	cout.tie(0);
	cin.tie(0);

	//setlocale(LC_ALL, "Ukr");
	read();
	parse();
	prep();
	used.resize(n+1);
	
	freopen("cripto.out", "w", stdout);
	bydlokod();
	/*
		int a[11] = {};
		bool used[11] = {};
		int n = 7;
		int i = 0;
		bool gone = false;
		int count = 0;

	next:
		if (i == n) {
			--i;
			//for (int j = 1; j <= n; ++j)
			//	cerr << a[j] << ' ';
			//cerr << '\n';
			++count;
		}

		gone = false;
		for (int j = a[i+1]+1; j <= 10; ++j) {
			if (!used[j]) {
				used[a[i + 1]] = false;
				a[i + 1] = j;
				used[j] = true;
				++i;
				gone = true;
				goto next;
			}
		}
		if (!gone && i != 0) {
			used[a[i + 1]] = false;
			a[i + 1] = 0;
			--i;
			goto next;
		}

		cerr << count << "\n";
	*/

	//dict = { 0, 8, 5, 0, 2, 9, 7, 6, 3, 1, 4 };
	//cout << check() << endl;

	return 0;
}
