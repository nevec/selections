/* GCC */

#include <cstdio>
#include <cstdlib>
#include <stdint.h>

#define MOD 41

int main()
{
    FILE *fin;
    int h[5] = {};
    int n, m, j;
    int64_t i, mask, dmask, imask;
    static int64_t d[5][25576] = {};

    fin = fopen("nested.in", "r");
    fscanf(fin, "%d %d", &n, &m);

    for (i = 0; i < n; ++i)
        fscanf(fin, "%d", &h[i]);

    fclose(fin);

    for (i = h[0]; i < (1 << m); i += MOD) {
        d[0][i / MOD] = 1;
    }

    for (j = 1; j < n; ++j) {
        for (i = h[j-1]; i < (1 << m); i += MOD) {
            mask = i;
            dmask = d[j-1][i / MOD];
            for (imask = mask; ; imask = (imask-1) & mask) {
                if (imask % MOD == h[j])
                    d[j][imask / MOD] += dmask;
                if (imask == 0)
                    break;
            }

        }

    }
    int64_t ans = 0;

    freopen("nested.out", "w", stdout);

    for (i = h[n-1]; i < (1 << m); i += MOD)
        ans += d[n-1][i/MOD];
    printf("%d\n", ans);
    return 0;
}
