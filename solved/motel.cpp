/* GCC */

#include <fstream>
#include <string>
#include <utility>
#include <stdint.h>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;

int gcd(int a, int b)
{
	a = abs(a);
	b = abs(b);
	while (a != 0 && b != 0) {
		if (a > b)
			a %= b;
		else
			b %= a;
	}
	return a + b;
}

struct Fract
{
	int den, num;
	Fract() : den(1), num(0) {}
	Fract(int n) : den(1), num(n) {}
	Fract(const Fract& f) : den(f.den), num(f.num) {}
	Fract(int64_t n, int64_t d)
	{
		int g = gcd(abs(n), abs(d));
		num = n / g;
		den = d / g;

		if (den < 0 && num < 0) {
			den *= -1;
			num *= -1;
		}
	}
	Fract operator+(const Fract& l) const
	{
		Fract t(den*l.num + l.den*num, den*l.den);
		return t;
	}
	Fract operator-(const Fract& l) const
	{
		return Fract(l.den*num - den*l.num, den*l.den);
	}
	Fract operator*(const Fract& l) const
	{
		return Fract(num*l.num, den*l.den);
	}
	Fract operator/(const Fract& l) const
	{
		return Fract(num*l.den, den*l.num);
	}
	Fract operator-() const
	{
		return Fract(-num, den);
	}
	bool operator<(const Fract& l) const
	{
		return (*this - l).num < 0;
	}
	bool operator>(const Fract& l) const
	{
		return (*this - l).num > 0;
	}
	bool operator==(const Fract& l) const
	{
		return (*this - l).num == 0;
	}

};


ostream& operator<<(ostream& o, const Fract& f)
{
	if (f.den == -1) {
		o << -f.num;
		return o;
	}
	o << f.num;
	if (f.den != 1)
		o << "/" << f.den;
	return o;
}

struct Point {
	Fract first, second;
	Point(Fract f, Fract s) : first(f), second(s) {}
};

bool operator<(const Point& l, const Point& r)
{
	if (!(l.first == r.first))
		return l.first < r.first;
	else
		return l.second < r.second;
}

bool operator==(const Point& l, const Point& r)
{
	return l.first == r.first && l.second == r.second;
}

Fract abs(Fract x) {
	return Fract(abs(x.num), abs(x.den));
}

Fract sqrt(Fract x)
{
	return Fract(sqrt(x.num), sqrt(x.den));
}

struct Line
{
	Fract a, b, c;
	Line(Fract x1, Fract y1, Fract x2, Fract y2)
	{
		Fract m = y2 - y1;
		Fract l = x2 - x1;
		a = m;
		b = -l;
		c = y1*l - x1*m;
	}

	void intersection(Line b, vector<Point>& pts) const
	{
		Fract d0 = det(this->a, this->b, b.a, b.b);
		Fract dx = det(-this->c, this->b, -b.c, b.b);
		Fract dy = det(this->a, -this->c, b.a, -b.c);

		if (d0.num == 0)
			return;
		if (find(pts.begin(), pts.end(), (Point(dx / d0, dy / d0))) == pts.end())
			pts.push_back(Point(dx / d0, dy / d0));
	}
	Fract distance(Point p) const
	{
		Fract n = a*p.first + b*p.second + c;
		if (n.num < 0)
			n = -n;
		if (n.den < 0)
			n.den = -n.den;

		Fract d = sqrt(a*a + b*b);
		return n / d;
	}
	Fract distance(Line l) const
	{
		Fract n = abs(c);
		Fract d = sqrt(a*a + b*b);

		Fract d1 = n / d;

		n = abs(l.c);
		d = sqrt(l.a*l.a + l.b*l.b);
		Fract d2 = n / d;

		return abs(d1 - d2);
	}
private:
	static Fract det(Fract a1, Fract a2, Fract b1, Fract b2) { return a1*b2 - a2*b1; }
};

void pretty_print(const Point& p, ostream& fout)
{
	fout << p.first << ' ' << p.second << endl;
}

int lcd(int a, int b) { return a / gcd(a, b)*b; }

void pretty_print(Line l, ostream& fout)
{
	int g = lcd(l.a.den, lcd(l.b.den, l.c.den));
	int a = l.a.num * g;
	int b = l.b.num*g;
	int c = l.c.num*g;
	if (a < 0 || (a == 0 && b < 0)) {
		a *= -1;
		b *= -1;
		c *= -1;
	}
	g = gcd(a, gcd(b, c));
	fout << a / g << ' ' << b / g << ' ' << c / g << endl;
}

int main()
{
	ifstream fin("motel.in");
	ofstream fout("motel.out");

	int n;
	fin >> n;
	vector<Line> ls;
	for (int i = 0; i < n; ++i) {
		int x1, x2, y1, y2;
		fin >> x1 >> y1 >> x2 >> y2;
		ls.push_back(Line(x1, y1, x2, y2));
	}
	vector<Point> pts;
	

	for (int i = 0; i < (int)ls.size(); ++i) {
		for (int j = i + 1; j < (int)ls.size(); ++j) {
			ls[i].intersection(ls[j], pts);
		}
	}

	vector<Point> points(pts.begin(), pts.end());

	if (!pts.empty()) {

		vector<pair<Fract, int> > dists;

		for (int i = 0; i < (int)pts.size(); ++i) {
			Fract sum_dist;
			for (int j = 0; j < (int)ls.size(); ++j) {
				sum_dist = sum_dist + ls[j].distance(pts[i]);
			}
			dists.push_back(make_pair(sum_dist, i));
		}

		sort(dists.begin(), dists.end());

		int i = 1;
		while (i < (int)dists.size() && dists[i].first == dists[0].first)
			++i;
		fout << dists[0].first << endl;
		fout << i << endl;
		for (int j = 0; j < i; ++j) {
			pretty_print(points[dists[j].second], fout);
		}
	}
	else {
		vector<pair<Fract, int> > dists;

		for (int i = 0; i < (int)ls.size(); ++i) {
			Fract dist_sum;

			for (int j = 0; j < (int)ls.size(); ++j) {
				dist_sum = dist_sum + ls[i].distance(ls[j]);
			}
			dists.push_back(make_pair(dist_sum, i));
		}
		sort(dists.begin(), dists.end());
		fout << dists[0].first << endl;
		if (!(dists[0].first == dists[1].first)) {

			fout << "-1\n";
			pretty_print(ls[dists[0].second], fout);
		}
		if (dists[0].first == dists[1].first) {
			fout << "-2\n";
			pretty_print(ls[dists[0].second], fout);
			pretty_print(ls[dists[1].second], fout);
		}
	}
	return 0;
}
