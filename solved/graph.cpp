/* GCC */

#include <cstdio>
#include <cstdlib>

#define MX 1000
#define SZ 1000

#define SZ2 499500

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))


struct queue
{
    short *m;
    int head;
    int tail;
    int size;
};

void queue_init(struct queue *q, int sz)
{
    q->head = q->tail = 0;
    q->m = new short[sz];
    q->size = sz;
}

void queue_delete(struct queue *q)
{
    delete[] q->m;
}

void queue_add(struct queue *q, short i)
{
    if (q->tail < q->size)
        q->m[q->tail++] = i;
    else {
        q->tail = 0;
        q->m[q->tail] = i;
    }
}

short queue_pop_back(struct queue *q)
{
    if (q->tail > 0)
        return q->m[--q->tail];
    else {
        q->tail = SZ - 1;
        return q->m[q->tail];
    }
}

short queue_pop_front(struct queue *q)
{
    if (q->head < q->size)
        return q->m[q->head++];
    else {
        q->head = 0;
        return q->m[q->head];
    }
}

int queue_empty(struct queue *q)
{
    return (q->head == q->tail);
}

short queue_front(struct queue *q)
{
    return q->m[q->head];
}

void read_g(int n, struct queue *gr, FILE *fin)
{
    int i;
    char temp[SZ*6];
    char *p, *e;
    short t;
    fgetc(fin);
    for (i = 0; i < n; ++i) {
        queue_init(gr + i, SZ);
        fgets(temp, SZ*6, fin);
        p = temp;
        while (1) {
            t = strtol(p, &e, 10);
            if (p == e)
                    break;
            queue_add(gr+i, t-1);
            p = e;
        }
    }
}



int main()
{
    static struct queue gr[MX];
    int n, i, foo, bar;
    static struct queue q;

    FILE *fin = fopen("graph.in", "r");
    FILE *fout = fopen("graph.out", "w");
    fscanf(fin, "%d", &n);
    read_g(n, gr, fin);

    queue_init(&q, SZ2);

    for (i = 0; i < n; ++i) {
        foo = queue_front(gr+i);
        if (i == queue_front(gr+foo) && i < foo)
            queue_add(&q, i);
    }

    while (!queue_empty(&q)) {
        foo = queue_pop_front(&q);

        if (queue_empty(gr+foo))
            continue;

        bar = queue_front(gr+foo);
        if (foo == queue_front(gr+bar)) {
            fprintf(fout, "%hd %hd\n", MIN(foo, bar)+1, MAX(foo, bar)+1);
            queue_pop_front(gr+foo);
            queue_pop_front(gr+bar);
            if (!queue_empty(gr+foo) && foo == queue_front(gr + queue_front(gr + foo))) {
                queue_add(&q, foo);
            }
            if (!queue_empty(gr+bar) && bar == queue_front(gr + queue_front(gr + bar))) {
                queue_add(&q, bar);
            }

        }
    }
    
    queue_delete(&q);
    for (i = 0; i < n; ++i)
        queue_delete(gr + i);
    fclose(fin);
    fclose(fout);
    
    return 0;
}
