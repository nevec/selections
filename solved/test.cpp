/* GCC */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <algorithm>
#include <stack>
#include <utility>
using namespace std;

#define NMAX 528
#define FMAX 248

short m[NMAX][NMAX] = {};

int maxi, maxj, mini, minj;
int k, num;

struct Figure
{
    int w, h, n, x0, y0, id;
} figs[FMAX];

void dfs(int i, int j)
{
    stack<pair<int, int> > s;
    s.push(make_pair(i, j));
    while (!s.empty())
    {
        i = s.top().first;
        j = s.top().second;
        s.pop();
        if (i < 0 || i >= NMAX || j < 0 || j >= NMAX)
            continue;
        if (m[i][j] != -1)
            continue;
        maxi = max(maxi, i);
        maxj = max(maxj, j);
        mini = min(mini, i);
        minj = min(minj, j);

        m[i][j] = k;
        ++num;
        s.push(make_pair(i + 1, j));
        s.push(make_pair(i - 1, j));
        s.push(make_pair(i, j - 1));
        s.push(make_pair(i, j + 1));
    }
}

void get_figure(short t[][NMAX], int f)
{
    for (int i = figs[f].y0; i < figs[f].y0+figs[f].h; ++i) {
        for (int j = figs[f].x0; j < figs[f].x0+figs[f].w; ++j) {
            if (m[i][j] == figs[f].id)
                t[i-figs[f].y0][j-figs[f].x0] = 1;
        }
    }
}

void horiz_axis_symm(short t1[][NMAX], Figure& f)
{
    for (int i = 0; i < f.w; ++i) {
        for (int j = 0, k = f.h-1; j < k; ++j, --k)
            swap(t1[j][i], t1[k][i]);
    }
}

void vert_axis_symm(short t1[][NMAX], Figure& f)
{
    for (int i = 0; i < f.h; ++i) {
        for (int j = 0, k = f.w-1; j < k; ++j, --k)
            swap(t1[i][j], t1[i][k]);
    }
}

bool check(short t1[][NMAX], short t2[][NMAX])
{
    return !memcmp(t1, t2, NMAX*NMAX*2);
}

void transp(short (t[][NMAX]), Figure& f)
{
    static short t2[NMAX][NMAX];
    memset(t2, 0, NMAX*NMAX*2);
    for (int i = 0; i < f.h; ++i)
        for (int j = 0; j < f.w; ++j)
            t2[j][i] = t[i][j];
    memcpy(t, t2, NMAX*NMAX*2);
    swap(f.w, f.h);
}

void show(short t[][NMAX], Figure f)
{
    for (int i = 0; i < f.h; ++i) {
        for (int j = 0; j < f.w; ++j) {
            printf("%d ", t[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}


bool compare(short t1[][NMAX], short t2[][NMAX], Figure f1, Figure f2)
{
    if (f1.w != f2.w || f1.h != f2.h || f1.n != f2.n)
        return false;
    //show(t1, f1);
    //show(t2, f2);
    if (check(t1, t2))
        return true;
    vert_axis_symm(t1, f1);
    if (check(t1, t2))
        return true;
    horiz_axis_symm(t1, f1);
    if (check(t1, t2))
        return true;
    vert_axis_symm(t1, f1);
    if (check(t1, t2))
        return true;
    horiz_axis_symm(t1, f1);
    return false;
}


int main()
{
    FILE *fin = fopen("test.in", "r"), *fout = fopen("test.out", "w");
    int n;
    fscanf(fin, "%d", &n);
    getc(fin);

    for (int i = 0; i < n; ++i)
    {
        static char line[NMAX];
        fgets(line, NMAX, fin);
        for (int j = 0; j < (int)strlen(line) - 1; ++j)
        {
            if (line[j] != ' ')
                m[i][j] = -1;
        }
    }

    k = 1;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (m[i][j] == -1)
            {
                num = 0;
                maxi = maxj = 0;
                mini = minj = INT_MAX;
                dfs(i, j);
                figs[k - 1].h = maxi - mini + 1;
                figs[k - 1].w = maxj - minj + 1;
                figs[k - 1].n = num;
                figs[k - 1].y0 = mini;
                figs[k - 1].x0 = minj;
                figs[k-1].id = k;
                ++k;
            }
        }
    }
    --k;

    int uniq = k;
    for (int i = 0; i < uniq; ++i)
    {
        for (int j = i + 1; j < uniq; ++j)
        {
            static short t1[NMAX][NMAX], t2[NMAX][NMAX];
            memset(t1, 0, NMAX*NMAX*2);
            memset(t2, 0, NMAX*NMAX*2);
            get_figure(t1, i);
            get_figure(t2, j);
            Figure f1 = figs[i], f2 = figs[j];
            //show(t1, f1);
            //show(t2, f2);
            //printf("----\n");
            if (compare(t1, t2, f1, f2)) {
                swap(figs[j], figs[uniq-1]);
                --j;
                --uniq;
                //printf("YES\n");
                continue;
            }
            transp(t1, f1);
            if (compare(t1, t2, f1, f2)) {
                swap(figs[j], figs[uniq-1]);
                --j;
                --uniq;
               // printf("YES\n");
                continue;
            }
        }
    }

    fprintf(fout, "%d %d\n", k, uniq);
    fclose(fin);
    fclose(fout);
    return EXIT_SUCCESS;
}
