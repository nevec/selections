/* GCC */

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;

typedef pair<int, int> cord;

#define X first
#define Y second


vector <cord> cords;
vector <int> v;
int w, h;

int upper(int y) {return distance(v.begin(), upper_bound(v.begin(), v.end(), y));}

struct DRM
{
private:
    vector<int> a;
public:
    DRM(int n) : a(n) { }
    int MAX() {return *max_element(a.begin(), a.end());}
    void UPDATE(int i, int j, int d) {for (int z = i; z <= j; ++z) a[z] += d; }
};

int f(int w, int h);
int mx = 0;

int main()
{
    int n;
    ifstream fin("mosquito.in");
    fin >> n;

    for (int i = 0; i < n; ++i) {
        int x, y;
        fin >> x >> y;
        mx = max(mx, max(x, y));
        cords.push_back(make_pair(x, y));
        v.push_back(y);
    }
    fin >> w >> h;
    fin.close();

    sort(v.begin(), v.end());
    sort(cords.begin(), cords.end());

    vector<int>::iterator uniq = unique(v.begin(), v.end());
    v.resize(distance(v.begin(), uniq));

    int mx = f(w, h);
    mx = max(mx, f(h, w));

    ofstream fout("mosquito.out");
    fout << mx << endl;
    fout.close();

    return 0;
}

int f(int w, int h)
{
    DRM S(100100) ;
    int left = 0;
    int xleft = cords[left].X;
    int right = 0;

    while (right < cords.size() && cords[right].X - xleft <= w) {
        S.UPDATE(upper(cords[right].Y), upper(cords[right].Y + h), +1);
        right++;
    }

    int opt = S.MAX();


    while (right < cords.size()) {

        while (left < cords.size() && cords[left].X == xleft) {
            S.UPDATE(upper(cords[left].Y), upper(cords[left].Y + h), -1);
            ++left;
        }
        xleft = cords[left].X;

        while (right < cords.size() && cords[right].X - xleft <= w) {
            S.UPDATE(upper(cords[right].Y), upper(cords[right].Y + h), +1);
            right++;
        }

        opt = max(opt, S.MAX());
    }
    return opt;
}
