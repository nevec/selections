/* GCC */

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
using namespace std;

typedef int64_t ll;

ll gcd(ll a, ll b)
{
	a = abs(a);
	b = abs(b);

	while (a != 0 && b != 0) {
		if (a > b)
			a %= b;
		else
			b %= a;
	}

	return (a + b);
}

struct Fract
{
	ll num, den;
	Fract() :num(0), den(0) {}
	Fract(ll n, ll d) {
		int g = gcd(n, d);
		if (d < 0) {
			d *= -1;;
			n *= -1;
		}
		num = n / g;
		den = d / g;
	}
};

Fract operator+(Fract a, Fract b) { return Fract(a.num*b.den + b.num*a.den, a.den*b.den); }
Fract operator-(Fract a, Fract b) { return Fract(a.num*b.den - b.num*a.den, a.den*b.den); }
Fract operator*(Fract a, Fract b) { return Fract(a.num*b.num, a.den*b.den); }
Fract operator/(Fract a, Fract b) { return Fract(a.num*b.den, a.den*b.num); }
Fract operator-(Fract a) { return Fract(-a.num, a.den); }
ostream& operator<<(ostream& o, Fract a) { o << a.num; if (a.den != 1) o << "/" << a.den; return o; }

int n, m;

vector<vector<Fract> > a;

int ANY = 0;

bool gauss()
{
	for (int r = 0; r < m; ++r) {
		int r1 = r - ANY;
		int id = -1;
		
		if (r1 == n)
			return true;

		if (count_if(a[r1].begin(), a[r1].end() - 1, [](Fract a) {return a.num == 0; }) == m - 1) {
			if (a[r1][m - 1].num != 0)
				return false;
			a.erase(a.begin() + r1);
			--n;
			--r;
			continue;
		}

		for (int i = r1; i < n; ++i) {
			if (a[i][r].num != 0) {
				id = i; 
				break;
			}
		}
		if (id == -1) {
			ANY++;
			continue;
		}
		a[r1].swap(a[id]);

		Fract val = a[r1][r];
		for (int i = 0; i < m; ++i) {
			a[r1][i] = a[r1][i] / val;
		}

		for (int i = 0; i < n; ++i) {
			if (i != r1) {
				val = a[i][r];

				for (int j = 0; j < m; ++j) {
					a[i][j] = a[i][j] - val*a[r1][j];
				}
			}
		}
	}

	return true;
}

int main()
{
	ifstream cin("linear.in");
	ofstream cout("linear.out");

	cin >> n >> m;
	++m;

	for (int i = 0; i < n; ++i) {
		a.push_back(vector<Fract>());
		for (int j = 0; j < m; ++j) {
			int t;
			cin >> t;
			a[i].push_back(Fract(t, 1));
		}
	}


	if (!gauss()) {
		cout << "No solution\n";
		return 0;
	}
	bool fail = false;

	for (int i = 0; i < n; ++i) {
		if (count_if(a[i].begin(), a[i].end() - 1, [](Fract v) {return v.num == 0; }) != m - 2) {
			fail = true;
			break;
		}
	}
	if (fail) {
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < m; ++j) {
				cout << " " << a[i][j];
			}
			cout << "\n";
		}
		return 0;
	}

	cout << " ";
	for (int i = 0; i < n; ++i) {
		cout << a[i][m - 1] << ((i != n - 1) ? " " : "\n");
	}


	return 0;
}