/* GCC */

#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstring>
#include <utility>
#include <iostream>
using namespace std;

int n;

const int Nmax = 200100;

int t[4 * Nmax] = {};

void update(int v, int l, int r, int pos, int x)
{
	if (l > r)
		return;
	if (l == pos && r == pos) {
		t[v] = x;
		return;
	}
	
	int m = (l + r) / 2;
	
	if (pos <= m)
		update(2 * v, l, m, pos, x);
	else
		update(2 * v + 1, m + 1, r, pos, x);

	t[v] = max(t[2 * v], t[2 * v + 1]);
}

int get_max(int v, int l, int r, int z)
{
	if (t[v] <= z)
		return -1;
	if (l == r)
		return l;
	int m = (l + r) / 2;
	if (t[2 * v] > z)
		return get_max(2 * v, l, m, z);
	else
		return get_max(2 * v + 1, m + 1, r, z);
}

int find_best(int v, int l, int r, int k, int z)
{
	if (k < l || k > r || l > r)
		return -1;
	if (l == r)
		return -1;
	int m = (l + r) / 2;
	if (m + 1 <= k && k <= r)
		return find_best(2 * v + 1, m + 1, r, k, z);
	int left = find_best(2 * v, l, m, k, z);
	if (left != -1)
		return left;
	else
		return get_max(2 * v + 1, m + 1, r, z);
}

void show(int n)
{
	for (int i = 0; i < n; ++i) {
		cerr << get_max(1, i, i, 0) <<" ";
	}
	cerr << endl;
}

int main()
{
	freopen("cclub.in", "r", stdin);
	freopen("cclub.out", "w", stdout);

	ios_base::sync_with_stdio(false);

	cin >> n;
	vector<pair<unsigned, unsigned> > players;
	vector<int> queries;
	fill(t, t + 4 * Nmax, 0);
	int pl_count = 0;

	for (int ii = 0; ii < n; ++ii) {
		char type;
		cin >> type;
		if (type == 'P') {
			unsigned age, level;
			cin >> age >> level;
			players.push_back(make_pair(age, level));
			queries.push_back(pl_count++);
		}
		else {
			int id;
			cin >> id;
			queries.push_back(-id);
		}
	}

	vector<int> x(players.size());
	for (int i = 0; i < x.size(); ++i)
		x[i] = i;
	sort(x.begin(), x.end(), [&](int l, int r) { 
		return players[l].second < players[r].second ||
			players[l].second == players[r].second && players[l].first < players[r].first; 
	});

	vector<int> ord(x.size());
	for (int i = 0; i < x.size(); ++i)
		ord[x[i]] = i;
	vector<int> sh(x.size());
	sh[x.size() - 1] = x.size() - 1;
	for (int i = x.size() - 2; i >= 0; --i) {
		if (players[x[i]].second == players[x[i + 1]].second)
			sh[i] = sh[i + 1];
		else
			sh[i] = i;
	}

	for (int i = 0; i < queries.size(); ++i) {
		int q = queries[i];
		if (q < 0) {
			//show(x.size());
			int id = ord[-q-1];
			id = sh[id];
			int ans = find_best(1, 0, Nmax, id, players[-q-1].first);
			if (ans == -1)
				cout << "0\n";
			else
				cout <<  x[ans]+1 << "\n";

		}
		else {
			int id = ord[q];
			update(1, 0, Nmax, id, players[q].first);
		}
	}

	return 0;
}
