/* GCC */

#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>
using namespace std;

int n;
int adj[17][17];
const int Inf = 1e9;

int t[1<<17][17]= {};

void f(int s)
{
    for (int i = 0; i < n; ++i)
        if (s & (1 << i)) {
            if (s == 1)
                t[s][i] = 0;
            for (int j = 0; j < n; ++j)
                if (i != j && (s & (1<<j)))
                    t[s][i] = min(t[s][i], t[s^(1<<i)][j] + adj[j][i]);
        }

    int m = 0;
    while (m != s) {
        int k = -1;
        int minv = Inf;
        for (int i=0;i<n;++i)
            if ((s & (1 << i)) && !(m & (1 << i)) && (t[s][i] < minv  || k == -1) ) {
                minv = t[s][i];
                k = i;
            }
        if (k == -1)
            break;
        m |= (1 << k);
        for (int i = 0; i < n; ++i)
            if (s&(1<<i) && !(m&(1<<i)))
                t[s][i] = min(t[s][i], t[s][k] + adj[k][i]);
    }
}

int main()
{
    freopen("vacation.in", "r", stdin);
    freopen("vacation.out", "w", stdout);
    scanf("%d\n", &n);
    ++n;

    for (int i = 0;i<n;++i){
        for (int j = 0;j<n;++j) {
            int t;
            scanf("%d", &t);
            adj[i][j] = (t>=0)?t:Inf;
        }
    }

    int minans = Inf;

    for (int j =0;j<(1<<n);++j)
       for (int i=0;i<17;++i)
         t[j][i] = Inf;

    t[0][0] = 0;

    for (int i = 1; i < (1 << n); ++i) {
        f(i);
    }

    for (int i = 2; i < (1 << n); ++i)
        if (t[i][0] != Inf )
        minans = min(minans, t[i][0] / (__builtin_popcount(i)-1) + 1);

    printf("%d\n", minans);

    return 0;
}
