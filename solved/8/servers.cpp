/* GCC */

#include <stdio.h>
#include <algorithm>
#include <utility>
#include <vector>
#include <stdlib.h>
#include <time.h>
using namespace std;

int n, m;
vector<pair<int, int> > p;

const int TimeLimit = 0.6*CLOCKS_PER_SEC;

int cost(const vector<int>& v)
{
    int c = 0;

    int pos[20];

    for (int i = 0; i < n; ++i)
        pos[v[i]] = i;

    for (int i = 0; i < m; ++i)
        c += abs(pos[p[i].first] - pos[p[i].second]);
    return c;
}

int main()
{
    clock_t t1 = clock();

    freopen("servers.in", "r", stdin);
    freopen("servers.out", "w", stdout);

    scanf("%d%d", &n, &m);
    for (int i=0; i<m; ++i)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        --a, --b;
        p.push_back(make_pair(a, b));
    }
    srand(time(0));

    vector<int> ans(n);
    int best_cost = 2e9;

    vector<int> perm;
    for (int i=0; i<n; ++i)
        perm.push_back(i);

    random_shuffle(perm.begin(), perm.end());

    while ((clock() - t1) < TimeLimit)
    {
        random_shuffle(perm.begin(), perm.end());
        bool has_better = true;
        int prev_cost;

        while ((clock() - t1) < TimeLimit && has_better)
        {
            prev_cost = cost(perm);
            has_better = false;

            for (int i =0; i< n; ++i)
            {
                for (int j = i+1; j < n; ++j)
                {
                    swap(perm[i], perm[j]);
                    int new_cost = cost(perm);
                    if (new_cost < prev_cost)
                    {
                        prev_cost = new_cost;
                        has_better = true;
                        break;
                    }
                    swap(perm[i], perm[j]);
                }
            }
        }
        if (best_cost > prev_cost)
        {
            best_cost = prev_cost;
            copy(perm.begin(), perm.end(), ans.begin());
        }
    }

    printf("%d\n", best_cost);
    for (int i=0; i<n; ++i)
    {
        printf("%d%s", ans[i]+1, ((i != n-1)?" ":"\n"));
    }

    return 0;
}
