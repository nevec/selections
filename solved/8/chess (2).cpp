/* GCC */

#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;

int V = 8, L = 4;
int n, m, d;
const int Vmax = 200+200;

int match[Vmax];
bool visited[Vmax];
bool adj[Vmax][Vmax];

bool dfs(int u)
{
    if (visited[u])
        return false;
    visited[u] = true;
    if (u >= L) {  // �� � �����
        if (match[u] == -1)
            return true;
        return dfs(match[u]);
    } else {
        for (int i = 0; i < V; ++i) {
            if (adj[u][i]) {
                int w = i;

                if (w != match[u]) {
                    if (dfs(w)) {
                        match[w] = u;
                        match[u] = w;
                        return true; // ��������
                    }
                }
            }
        }
        return false;  // �����
    }
}

int PREV_ANS = 0;

int max_pairmatch() // ����������� ��������������
{

    int size = PREV_ANS;

    while (true) {
        bool improved = false;
        fill(visited, visited + V, false);
        for (int i = 0; i < L; ++i) {
            if (match[i] == -1 && dfs(i)) {
                improved = true;
                size++;
                //break;
            }
        }
        if (!improved)
            break;
    }
    return size;
}

int kiev[210], selo[210];

int do_try(int D)
{
 /*   for (int i = 0; i < L; ++i)
        adj[i].clear();*/
   // 
 /*   
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (abs(selo[i] + D - kiev[j]) <= d)
                adj[i].push_back(L + j);
        }
    }*/
    
    return PREV_ANS = max_pairmatch();
}

struct event
{
    int i, j, value, type;
    event():i(0), j(0), value(0), type(0) {}
    event(int i, int j, int v, int t) : i(i),j(j),value(v), type(t) {}
};

bool operator<(event l, event r) {return l.value < r.value || (l.value == r.value && l.type < r.type); }

int main()
{
    freopen("chess.in", "r", stdin);
    freopen("chess.out", "w", stdout);

    scanf("%d%d%d", &n, &m, &d);
    for (int i = 0; i < n; ++i)
        scanf("%d", &selo[i]);
    for (int i = 0; i < m; ++i)
        scanf("%d", &kiev[i]);
    L = n, V = n + m;

    int ln = *min_element(selo, selo + n),
        lx = *max_element(selo, selo + n),
        Ln = *min_element(kiev, kiev + m),
        Lx = *max_element(kiev, kiev + m);

    int lower = Ln - lx + d, upper = Lx - ln - d;
    if (lower > upper) {
        int ans = do_try(lower);
        printf("%d %d\n", ans, lower);
        for (int i = 0; i < L; ++i)
            if (match[i] != -1)
                printf("%d %d\n", i + 1, match[i] + 1);
        
    } else {
        fill(match, match+V, -1);
        int best = 0, bestd = -1;
        int bestm[200] = {};
        vector<event> v;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                v.push_back(event(i, j, kiev[j] - selo[i] - d, 0));
                v.push_back(event(i, j, kiev[j] - selo[i] + d, 1));
            }
        }
        
        sort(v.begin(), v.end());
        
        for (int i =0; i < v.size(); ) {
            while ( i < v.size() && v[i].type == 1) {
                adj[v[i].i][L+v[i].j] = false;
                if (match[v[i].i] == L+v[i].j) {
                    match[v[i].i] = -1;
                    match[L+v[i].j] = -1;
                    PREV_ANS--;
                }
                ++i;
            }
            int k = 0;
            while (i < v.size() && v[i].type == 0) {
                adj[v[i].i][L+v[i].j] = true;
                ++i;
                ++k;
            }
            if (k <= 2  && L > 100)
                continue;
            int ans = do_try(-1);
            if (ans > best) {
                best = ans;
                bestd = v[i].value;
                memcpy(bestm, match, 4*L);
            }
        }
        
        printf("%d %d\n", best, bestd);
        for (int i = 0; i < L; ++i)
            if (bestm[i] != -1)
                printf("%d %d\n", i + 1, bestm[i]-L + 1);
    }

    return 0;
}
