/* GCC */

#include <cstdio>
#include <vector>
#include <queue>
#include <algorithm>
using namespace std;

int m,n;
const int Nmax = 256;
const int Inf = 1e9;
const int Horizontal = 0, Vertical = 1;
int dt[][2] = {{0, 1}, {1, 0}, {-1, 0}, {0, -1}};

struct Edge {
    int v;
    int time;
    int type;
    Edge(): v(-1), time(-1), type(-1) {}
    Edge(int v, int ti, int ty) : v(v), time(ti), type(ty) {}
};

int time[Nmax][Nmax];
vector<Edge> graph[4*Nmax*Nmax];
int dist[4*Nmax*Nmax];
inline bool is_valid(int y, int x) { return y >= 0 && y < 2*n && x >= 0 && x < 2*m; }

inline int get_id(int y, int x) { return y*2*m+x; }

void dejkstra(int s)
{
    priority_queue<pair<int, int> > q;
    q.push(make_pair(0, s));

    fill(dist, dist + 4*Nmax*Nmax, Inf);
    dist[s] = 0;

    while (!q.empty()) {
        int d = -q.top().first;
        int v = q.top().second;
        q.pop();

        if (dist[v] < d)
            continue;

        for (int i = 0; i < graph[v].size(); ++i) {
            Edge e = graph[v][i];

            int new_dist = d;
            if (e.time == -1) {
                new_dist += 60;
            } else if ((d / e.time) % 2 == e.type)
                new_dist += 1;
            else
                new_dist = (new_dist/e.time+1)*e.time+1;

            if (dist[e.v] > new_dist) {
                dist[e.v] = new_dist;
                q.push(make_pair(-new_dist, e.v));
            }
        }
    }
}

int main()
{
    freopen("school.in", "r", stdin);
    freopen("school.out", "w", stdout);
    scanf("%d%d", &m, &n);
    for (int i=n-1; i >= 0; --i)
        for (int j=0; j<m; ++j)
            scanf("%d", &time[i][j]);

    for (int i = 0; i < 2*n; ++i) {
        for (int j = 0; j < 2*m; ++j) {
            int y = i, x = j;
            for (int k = 0; k < 4; ++k) {
                int ny = y + dt[k][0];
                int nx = x + dt[k][1];
                if (is_valid(y, x) && is_valid(ny, nx)) {
                    int a = get_id(y, x);
                    int b = get_id(ny, nx);
                    if (x == nx) {
                        if (y/2 != ny/2)
                            graph[a].push_back(Edge(b, -1, Vertical));
                        else
                            graph[a].push_back(Edge(b, time[ny/2][nx/2], Vertical));
                    } else {
                        if ( x/2 != nx/2)
                            graph[a].push_back(Edge(b, -1, Horizontal));
                        else
                            graph[a].push_back(Edge(b, time[ny/2][nx/2], Horizontal));
                    }
                }
            }
        }
    }

    dejkstra(0);
    printf("%d\n", dist[4*n*m-1]);

    return 0;
}
