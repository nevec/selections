/* GCC */

#include <cstdio>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

struct Point
{
    double x, y;
    Point() : x(0.0), y(0.0) {}
    Point(double x, double y) : x(x), y(y) {}
};

Point operator-(Point a, Point b) {return Point(a.x-b.x, a.y-b.y); }
Point operator+(Point a, Point b) {return Point(a.x+b.x, a.y+b.y); }
Point operator*(Point a, double k) {return Point(k*a.x, k*a.y); }
double operator^(Point a, Point b) {return a.x*b.y-a.y*b.x; }

double dist(Point a, Point b) {return sqrt(pow(a.x-b.x, 2) + pow(a.y-b.y, 2)); }
double dist(Point a) {return sqrt(pow(a.x, 2) + pow(a.y, 2)); }

void normalize(Point& a, double len){ a = a * (len/dist(a)); }

const double Open = 1.0, Close = 2.0;
const double eps = 1e-10;

int main()
{
    freopen("circle.in", "r", stdin);
    freopen("circle.out", "w", stdout);

    int n, k;
    scanf("%d%d", &n, &k);
    Point a[200];

    for (int i=0;i<n;++i)
        scanf("%lf%lf", &a[i].x, &a[i].y);

    double left = 0, right = 1e4;

    for (int kk = 0; kk < 35; ++kk) {
        double r = (left+right)/2.0;
        //r = 2.0;
        int maxc = 1;

        for (int j = 0; j < n; ++j) {
            Point A = a[j];
            static pair<double, double> v[40000];
            int nv = 0;
            int c = 1;

            for (int i = 0; i < n; ++i) {
                if (i != j && dist(A, a[i]) - eps <= 2*r) {
                    Point vAB = a[i] - A;
                   // cerr << vAB.x << " " << vAB.y << endl;
                    vAB = (vAB*0.5);
                   // cerr << vAB.x << " " << vAB.y << endl;

                    Point n1(-vAB.y, vAB.x), n2(vAB.y, -vAB.x);
                    double katet = dist(vAB);
                    double hyp = r;
                    double len = sqrt(pow(hyp, 2) - pow(katet, 2));

                    normalize(n1, len);
                    normalize(n2, len);

                    Point a = vAB+n1, b = vAB+n2;
                    if ((a ^ b) < 0)
                        swap(a, b);
                    double alpha = atan2(a.y, a.x),
                        beta = atan2(b.y, b.x);
                    if (alpha > beta)
                        ++c;
                   // cerr << fixed;
                    //cerr << 180*alpha/3.14 << " " << 180*beta/3.14 << endl;
                    v[nv++] = make_pair(alpha, Open);
                    v[nv++] = make_pair(beta, Close);
                }
            }
            sort(v, v + nv);

            for (int i =0; i < nv; ++i)
                if (v[i].second == Open) {
                    ++c;
                    maxc = max(maxc, c);
                } else
                    --c;
        }
        if (maxc >= k) {
                right = r;
        } else
                left = r;
    }
    printf("%.3f\n", left);
    return 0;
}
