#include <fstream>
#include <iomanip>
using namespace std;

int fastPow(int a, int n, int m)
{
	int ans = 1;
	while (n) {
		if (n & 1)
			ans = (ans * a) % m;
		a = (a*a) % m;
		n >>= 1;
	}
	return ans;
}

int B(int p, int k)
{
	int ans = 1;
	int pk = fastPow(p, k, 1e9);
	for (int i = 1; i < pk; ++i) {
		if (i % p != 0)
			ans = (ans * i) % pk;
	}
	return ans;
}

int S(int M, int p, int k)
{
	int ans = 1;
	int pk = fastPow(p, k, 1e9);
	for (int i = 1; i <= M; ++i) {
		if (i % p != 0) {
			ans = (ans*i) % pk;
		}
	}
	return ans;
}

int calcMod(int N, int p, int k)
{
	if (N == 0)
		return 1;
	int ans = 1;
	int pk = fastPow(p, k, 1e9);
	ans = (ans*fastPow(B(p, k), N / pk, pk)) % pk;
	ans = (ans * S(N % pk, p, k)) % pk;
	ans = (ans * calcMod(N / p, p, k)) % pk;

	return ans;
}

int countFactrl(int n, int p)
{
	int ans = 0;

	for (int i = p; i <= n; i *= p)
		ans += n / i;
	return ans;
}

int main()
{
	ifstream cin("factrl.in");
	ofstream cout("factrl.out");

	int N;
	cin >> N;
	int A0 = calcMod(N, 2, 3);
	int B0 = calcMod(N, 5, 3);
	int m = countFactrl(N, 2),
		n = countFactrl(N, 5);
	int complA = fastPow(fastPow(5, n, 8), 3, 8),
		complB = fastPow(fastPow(2, m, 125), 99, 125);

	int A = (A0 * complA) % 8;
	int B = (B0 * complB) % 125;

	for (int i = 0; i < 1000; ++i) {
		if (i % 8 == A && i % 125 == B)
			cout << setfill('0') << setw(3) << (i*fastPow(2, m - n, 1000)) % 1000 << "\n";
	}

	return 0;
}