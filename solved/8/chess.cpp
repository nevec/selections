/* GCC */

#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

int A[81][81] = {}, B[81] = {};
int p;

void decode(int pos, bool state[][2])
{
	for (int i = 0; i < 4; ++i) {
		int shift = pos % 3;
		if (shift == 1) {
			state[i][0] = true;
		}
		else if (shift == 2) {
			state[(i + 2) % 4][1] = true;
		}
		pos /= 3;
	}
}

bool can_move(int j, int k)
{
	bool prev[4][2] = {}, next[4][2] = {};
	decode(j, prev);
	decode(k, next);

	for (int i = 0; i < 4; ++i) {
		if (prev[i][1] != next[i][0] ||
			((i > 0) && prev[i][0] && next[i - 1][1]) ||
			((i < 3) && prev[i][0] && next[i + 1][1]))
			return false;
	}
	return true;
}

void multiply(int ans[][81], int b[][81], int c[][81])
{
	static int a[81][81];
	memset(a, 0, 4 * 81 * 81);

	for (int i = 0; i < 81; ++i) {
		for (int j = 0; j < 81; ++j) {
			for (int k = 0; k < 81; ++k) {
				a[i][k] = (a[i][k] + (((unsigned long long)b[i][j] * c[j][k]) % p))%p;
			}
		}
	}
	memcpy(ans, a, 4 * 81 * 81);
}

void fast_pow(int a[][81], int n)
{
	int i = 1;

	static int ans[81][81], temp[81][81];
	memset(ans, 0, 4 * 81 * 81);
	memcpy(temp, a, 4 * 81 * 81);

	for (int i = 0; i < 81; ++i)
		ans[i][i] = 1;

	for (int j = 1; j <= n; j *= 2) {
		if (n & j)
			multiply(ans, ans, temp);

		multiply(temp, temp, temp);
	}

	memcpy(a, ans, 4 * 81 * 81);
}

int main()
{
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);

	for (int i = 0; i < 81; ++i) {
		for (int j = 0; j < 81; ++j) {
			if (can_move(i, j)) {
				A[i][j] = 1;
			}
		}
		B[i] = 1;
	}

	int n;
	scanf("%d%d", &n, &p);

	if (n == 2) {
		printf("%d\n",  81 % p);
		return EXIT_SUCCESS;
	}

	fast_pow(A, n - 2);

	int ans[81] = {};

	for (int i = 0; i < 81; ++i) {
		for (int j = 0; j < 81; ++j) {
			ans[i] += (((long long)A[i][j] * B[j])%p);
			ans[i] %= p;
		}
	}

	int res = 0;
	for (int i = 0; i < 81; ++i) {
		res += (ans[i] % p);
		res %= p;
	}
	printf("%d\n", res);

	return EXIT_SUCCESS;
}