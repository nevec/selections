/* GCC */

#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>

using namespace std;

int gcd(int a, int b)
{
	while (a != 0 && b != 0) {
		if (a > b)
			a %= b;
		else
			b %= a;
	}

	return a + b;
}

vector<int64_t> bigint(1, 0);

int get_len()
{
	return bigint.size();
}

void mult(int x)
{
	if (x == 1)
		return;
	int len = get_len();
	int64_t shift = 0;

	for (int i = 0; i < len; ++i) {
		int64_t val = (x*bigint[i] + shift);
		bigint[i] = val % 10;
		shift = val / 10;
	}

	while (shift != 0) {
		bigint.push_back(shift % 10);
		shift /= 10;
	}
}

void show()
{
	int len = get_len();

	for (int i = len - 1; i >= 0; --i) {
		printf("%d", bigint[i]);
	}
	printf("\n");
}

int main()
{
	vector<int> perm;
	freopen("sleep.in", "r", stdin);
	freopen("sleep.out", "w", stdout);

	int n;
	scanf("%d", &n);

	for (int i = 0; i < n; ++i) {
		int t;
		scanf("%d", &t);
		perm.push_back(t);
	}

	vector<bool> used(n, false);


	vector<int> cycles;

	for (int i = 0; i < n; ++i) {
		if (!used[i]) {
			int len = 0;
			int v = i;
			while (!used[v]) {
				used[v] = true;
				v = perm[v] - 1;
				++len;
			}
			cycles.push_back(len);
		}
	}
	//cycles = { 4, 2, 5, 15 };

	for (int i = 0; i < (int)cycles.size(); ++i) {
		for (int j = i + 1; cycles[i] != 1 && j < (int)cycles.size(); ++j) {
			cycles[j] /= gcd(cycles[i], cycles[j]);
		}
	}

	bigint[0] = 1;

	for (int i = 0; i < (int)cycles.size(); ++i) {
		mult(cycles[i]);
	}

	show();

	return EXIT_SUCCESS;
}