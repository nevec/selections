/* GCC */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 110

int dp[MAX][MAX], vals[MAX][MAX], prev[MAX][MAX];

void solve(int n, int m)
{
    int i, j, max, max_index, coins;

    for (i = 1; i <= n; ++i) {
        dp[i][0] = vals[i][0];
        prev[i][0] = i;
        for (j = 1; j < m; ++j) {
            max = 0;
            max_index = -1;
            for (coins = 0; coins <= i; ++coins)
                if (dp[i - coins][j - 1] + vals[coins][j] > max) {
                    max = dp[i - coins][j - 1] + vals[coins][j];
                    max_index = coins;
                }
            dp[i][j] = max;
            prev[i][j] = max_index;
        }
    }
}

void restore_path(int path[], int n, int m)
{
    int bank = m - 1, cur_coin = n;

    while (bank >= 0) {
        if (!prev[cur_coin][bank])
            bank--;
        else {
            path[bank] = prev[cur_coin][bank];
            cur_coin -= prev[cur_coin][bank];

            bank--;
        }
    }
}

int main(void)
{
    FILE *fout = fopen("profit.out", "w");
    FILE *fin = fopen("profit.in", "r");
    int n, m, i, j, path[MAX];

    fscanf(fin, " %d %d", &n, &m);

    for (i = 0; i < n; ++i)
        for (j = 0; j < m; ++j)
            fscanf(fin, " %d", &vals[i + 1][j]);

    solve(n, m);
    memset(path, 0, sizeof(path));
    restore_path(path, n, m);

    fprintf(fout, "%d\n", dp[n][m - 1]);
    fprintf(fout, "%d", path[0]);
    for (i = 1; i < m; ++i)
        fprintf(fout, " %d", path[i]);
    fprintf(fout, "\n");

    fclose(fout);
    fclose(fin);
    return EXIT_SUCCESS;
}
