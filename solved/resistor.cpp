/* GCC */

#include <vector>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <stdint.h>

using namespace std;

const int Size = 2500;

int64_t gcd(int64_t a, int64_t b)
{
    while (a != 0 && b != 0) {
        if (a > b)
            a %= b;
        else
            b %= a;
    }

    return a+b;
}

struct Fract
{
    Fract() : den(0), num(0) {}
    Fract(int64_t n, int64_t d) : den(d/gcd(d, n)), num(n/gcd(d, n)) {}
    Fract operator+(const Fract& f) const
    {
        int n = num*f.den + den*f.num;
        int d = den*f.den;
        return Fract(n, d);
    }
    Fract operator|(const Fract& f) const
    {
        int n = num*f.num;
        int d = num*f.den + den*f.num;
        return Fract(n, d);
    }
    void show() const
    {
        printf("%d", num);
        if (den != 1)
            printf("/%d", den);
        printf("\n");
    }
    int den, num;
};

typedef vector<vector<Fract> > graph;
typedef vector<int> vi;

int n, m;

bool zero_way(graph& g)
{
    bool used[Size] = {};
    queue<int> q;
    q.push(0);
    used[0] = true;

    while (!q.empty()) {
        int vert = q.front();
        q.pop();

        for (int i = 0; i < n; ++i) {
            if (g[vert][i].den != 0 && g[vert][i].num == 0 && !used[i]) {
                q.push(i);
                used[i] = true;
            }
        }
    }

    return used[n-1];
}

bool has_way(graph& g)
{
    static bool used[Size] = {};
    queue<int> q;
    q.push(0);
    used[0] = true;

    while (!q.empty()) {
        int vert = q.front();
        q.pop();

        for (int i = 0; i < n; ++i) {
            if (g[vert][i].den != 0 && !used[i]) {
                q.push(i);
                used[i] = true;
            }
        }
    }

    return used[n-1];
}

struct Has_edge
{
    bool operator()(const Fract& f) {return f.den != 0;}
};

void count_edges(graph& g, vi& edges)
{
    for (int i = 0; i < n; ++i) {
        edges[i] = count_if(g[i].begin(), g[i].end(), Has_edge());
    }
}

void del_ends(graph& g, vi& edges)
{
    queue<int> q;

    for (int i = 1; i < n-1; ++i)
        if (edges[i] == 1)
            q.push(i);

    while (!q.empty()) {
        int vert = q.front();
        q.pop();

        int to = distance(g[vert].begin(), find_if(g[vert].begin(), g[vert].end(), Has_edge()));
        //fprintf(stderr, "%d\n", to);

        g[vert][to] = g[to][vert] = Fract();
        edges[to]--;
        if (edges[to] == 1 && to != 0 && to != n-1)
            q.push(to);
    }
}

void simplify(graph& g, vi& edges)
{
    bool fl = true;

    while (fl) {
        fl = false;
        queue<int> q;
        for (int i = 0; i < n; ++i) {
            if (edges[i] == 2 && i != 0 && i != n-1)
            {
                fl = true;
                q.push(i);
            }
        }

        while (!q.empty()) {
            int v = q.front();
            q.pop();


            vector<int> temp;
            for (int i = 0; i < n; ++i) {
                if (g[v][i].den != 0)
                    temp.push_back(i);
            }

            int to1 = temp[0], to2 = temp[1];

            Fract r = g[to1][v] + g[v][to2];

            if (g[to1][to2].den == 0)
                g[to1][to2] = r;
            else
                g[to1][to2] = g[to1][to2] | r;

            g[to2][to1] = g[to1][to2];

            g[to1][v] = g[v][to2] = g[v][to1] = g[to2][v]= Fract();
            edges[v] = 0;
        }
    }

}

int main()
{
    freopen("resistor.in", "r", stdin);
    freopen("resistor.out", "w", stdout);

    scanf("%d%d", &n, &m);

    graph g;
    g.resize(n, vector<Fract>(n));

    for (int i = 0; i < m; ++i) {
        int p1, p2, dn, nm;
        scanf("%d%d%d%d", &p1, &p2, &nm, &dn);
        --p1;
        --p2;
        if (g[p1][p2].den == 0)
            g[p1][p2] = Fract(nm, dn);
        else
            g[p1][p2] = Fract(nm, dn) | g[p1][p2];

        g[p2][p1] = g[p1][p2];
    }

    if (zero_way(g)) {
        printf("0\n");
        return 0;
    }
    if (!has_way(g)) {
        printf("Zero conductivity\n");
        return 0;
    }

    vector<int> edges(n);

    count_edges(g, edges);

    //del_ends(g, edges);

    simplify(g, edges);


    g[0][n-1].show();
    return 0;
}
