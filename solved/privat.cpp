/* GCC */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <vector>

using namespace std;

typedef vector<vector<int> > mtx;

const int Up = 1, Right = 2, End = 3;

int main()
{	
	freopen("privat.in", "r", stdin);
	freopen("privat.out", "w", stdout);

	mtx vals, dp, path;
	int m, n;
	scanf("%d%d", &m, &n);
	dp.resize(m, vector<int>(n));
	path.resize(m, vector<int>(n));
	
	for (int i = 0; i < m; ++i) {
		vals.push_back(vector<int>());
		for (int j = 0; j < n; ++j) {
			int t;
			scanf("%d", &t);
			vals[i].push_back(t);
		}
	}

	for (int i = 0; i < m; ++i) {
		path[i][n - 1] = End;
		dp[i][n - 1] = vals[i][n - 1];
	}

	for (int i = 0; i < n; ++i) {
		path[0][i] = End;
		dp[0][i] = vals[0][i];
	}

	for (int i = 1; i < m; ++i) {
		for (int j = n - 2; j >= 0; --j) {
			path[i][j] = Up;
			int y = i-1, x = j;

			if (dp[i][j + 1] > dp[i - 1][j]) {
				path[i][j] = Right;
				++y;
				++x;
			}

			int profit;
			switch (path[y][x]) {
			case Up:
				profit = dp[y - 1][x];
				break;
			case Right:
				profit = dp[y][x + 1];
				break;
			case End:
				profit = 0;
			}
			dp[i][j] = profit + vals[i][j];
		}
	}

	int y = m - 1, x = 0;

	vector<char> pth;

	bool set1, set2 = set1 = false;

	int profit1 = 0, profit2 = 0;

	while (path[y][x] != End) {
		switch (path[y][x]) {
		case Up:
			--y;
			pth.push_back('u');
			break;
		case Right:
			pth.push_back('r');
			++x;
			break;
		case End:
			break;
		}

		if (!set1) {
			profit1 = dp[y][x];
			set1 = true;
			continue;
		}
		if (!set2) {
			profit2 = dp[y][x];
			set2 = true;
			continue;
		}
	}

	printf("%d %d\n", profit1, profit2);

	for (int i = 0; i < (int)pth.size(); ++i) {
		printf("%c", pth[i]);
	}
	printf("\n");


	return EXIT_SUCCESS;
}