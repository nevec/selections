/* GCC */

#include <fstream>
#include <iostream>
#include <cmath>

using namespace std;

typedef long double extended;

const extended PI = 3.14159265358979323846L;

class vector
{
public:
    vector(extended x, extended y, extended z) : x(x),y(y), z(z) {}
    vector() : x(0), y(0),z(0) {}
    vector operator*(const vector& a) const
    {
        extended x_ = y * a.z - z * a.y;
        extended y_ = z * a.x - x * a.z;
        extended z_ = x * a.y - y * a.x;
        return vector(x_,y_,z_);
    }
    extended getEdge(const vector& a) const
    {
        extended scalar = x * a.x + y * a.y + z * a.z;
        extended mod1 = sqrt(x*x + y*y + z*z);
        extended mod2 = sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
        return scalar / (mod1 * mod2);
    }
    int getOrient(const vector& a) const
    {
        extended scalar = x * a.x + y * a.y + z * a.z;
	return (scalar > 0)?1:-1;
    }
    friend ostream& operator<<(ostream& o, const vector& v);
    friend istream& operator>>(istream& i, vector& v);
private:
    extended x;
    extended y;
    extended z;
};

ostream& operator<<(ostream& o, const vector& v)
{
    o.precision(3);
    o.setf(ios_base::fixed);
    o << v.x << ' ' << v.y << ' ' << v.z << endl;
    return o;
}

istream& operator>>(istream& i, vector& v)
{
    extended g, m, s;
	int gi, mi, si;
    i >> gi >> mi >> si;
	g = gi;
	m = mi;
	s = si;
    extended lattitude = (PI * (g + (m + s / 60.L) / 60.L)) / 180.L;
    i >> gi >> mi >> si;
	g = gi;
	m = mi;
	s = si;
    extended longtitude = (PI * (g + (m + s / 60.L) / 60.L)) / 180.L;
    v.z = sin(lattitude);
    v.x = cos(lattitude) * cos(longtitude);
    v.y = cos(lattitude) * sin(longtitude);
    return i;
}

extended arccos(extended a)
{
   if (fabs(a + 1) < 1e-16)
        a = -1; 
   return acos(a);

}

extended calculate(const vector& prev, const vector& a, const vector& next)
{
    vector u = (a * prev) * a;
    vector w = (a * next) * a;
    vector temp = a*u;
    extended e1 = w.getEdge(u);
    extended e2 = w.getOrient(temp);
    if (fabs(e1 - 0) < 1e-10 && e2 > 0)
        return PI/2;
    else if (fabs(e1 - 0) < 1e-10 && e2 < 0)
        return 3*PI/2;
    else if (e1 < 0 && e2 > 0)
        return arccos(e1);
    else if (e1 > 0 && e2 > 0)
        return arccos(e1);
    else if (e1 < 0 && e2 < 0)
        return 2*PI - arccos(e1);
    else if (e1 > 0 && e2 < 0)
        return 2*PI - arccos(e1);


}



int main()
{
    ifstream fin("riman2.in");
    ofstream fout("riman2.out");
    fout.precision(9);
    fout.setf(ios_base::fixed);
    vector a, b;
    int n = 0;
    fin >> a >> b;
    vector prev, i, next;
    prev = a;
    i = b;
    extended sum = 0.L;
    while (fin >> next)
    {

        extended temp = calculate(prev, i, next);
        //cerr << temp << endl;
        sum += temp;
        prev = i;
        i = next;
        n++;
    }

    sum += calculate(prev, i, a);
    sum += calculate(i, a, b);
    sum -= n*PI;

    if (sum < 2*PI)
        fout << (sum) / (4*PI - sum) << endl;
    else
       fout << (4*PI - sum) / sum << endl;



    return 0;
}
